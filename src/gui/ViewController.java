package gui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import util.Calculo;

public class ViewController implements Initializable {

	private String[] tipoJuros = { "SIMPLES", "COMPOSTO" };
	private String[] tipoDeterminar = { "FV", "PV", "i", "n" };
	private double valorPresente;
	private double valorFuturo;
	private double taxaJuros;
	private double periodo;

	@FXML
	private TextField txtPv;

	@FXML
	private TextField txtFv;

	@FXML
	private TextField txtTaxa;

	@FXML
	private Spinner<Integer> spPeriodo;

	@FXML
	private Button btnCalcula;

	@FXML
	private Button btnLimpa;

	@FXML
	private TextArea txtResultado;

	@FXML
	private ChoiceBox<String> cbTipo;

	@FXML
	private ChoiceBox<String> cbDeterminar;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 50);
		valueFactory.setValue(5);
		spPeriodo.setValueFactory(valueFactory);
		periodo = spPeriodo.getValue();

		cbTipo.getItems().addAll(tipoJuros);
		cbTipo.setValue(tipoJuros[0]);

		cbDeterminar.getItems().addAll(tipoDeterminar);
		cbDeterminar.setValue(tipoDeterminar[0]);
		cbDeterminar.setOnAction(this::getDeterminar);

		txtPv.setDisable(false);
		txtFv.setDisable(true);
		txtTaxa.setDisable(false);
		spPeriodo.setDisable(false);
	}

	public void getDeterminar(ActionEvent event) {
		String valor = cbDeterminar.getValue().toString();
		switch (valor) {
			case "PV":
				txtPv.setDisable(true);
				txtFv.setDisable(false);
				txtTaxa.setDisable(false);
				spPeriodo.setDisable(false);
				break;

			case "FV":
				txtPv.setDisable(false);
				txtFv.setDisable(true);
				txtTaxa.setDisable(false);
				spPeriodo.setDisable(false);
				break;

			case "i":
				txtPv.setDisable(false);
				txtFv.setDisable(false);
				txtTaxa.setDisable(true);
				spPeriodo.setDisable(false);
				break;

			case "n":
				txtPv.setDisable(false);
				txtFv.setDisable(false);
				txtTaxa.setDisable(false);
				spPeriodo.setDisable(true);
				break;

			default:
				throw new IllegalArgumentException("Unexpected value: " + cbDeterminar.getValue());
		}
	}

	public void calcularValor() {
		txtResultado.clear();
		txtResultado.appendText("JUROS " + cbTipo.getValue() + "\n");

		try {
			switch (cbDeterminar.getValue()) {
				case "FV":
					txtResultado.appendText("Determinando o valor futuro\n");
					valorPresente = Double.parseDouble(txtPv.getText().replace(',', '.'));
					periodo = spPeriodo.getValue();
					taxaJuros = Double.parseDouble(txtTaxa.getText().replace(',', '.')) / 100.0;

					valorFuturo = (cbTipo.getValue().equals("SIMPLES"))
							? Calculo.juroSimplesDeterminarValorFuturo(valorPresente, taxaJuros, periodo)
							: Calculo.juroCompostoDeterminarValorFuturo(valorPresente, taxaJuros, periodo);

					txtResultado.appendText("FV = " + String.format("%.2f", valorFuturo) + "\n");
					break;

				case "PV":
					txtResultado.appendText("Determinando o valor presente\n");
					valorFuturo = Double.parseDouble(txtFv.getText().replace(',', '.'));
					periodo = spPeriodo.getValue();
					taxaJuros = Double.parseDouble(txtTaxa.getText().replace(',', '.')) / 100.0;

					valorPresente = (cbTipo.getValue().equals("SIMPLES"))
							? Calculo.juroSimplesDeterminarValorPresente(valorFuturo, taxaJuros, periodo)
							: Calculo.juroCompostoDeterminarValorPresente(valorFuturo, taxaJuros, periodo);

					txtResultado.appendText("PV = " + String.format("%.2f", valorPresente) +
							"\n");
					break;

				case "i":
					txtResultado.appendText("Determinando a taxa de juros\n");
					valorPresente = Double.parseDouble(txtPv.getText().replace(',', '.'));
					valorFuturo = Double.parseDouble(txtFv.getText().replace(',', '.'));
					periodo = spPeriodo.getValue();

					taxaJuros = (cbTipo.getValue().equals("SIMPLES"))
							? Calculo.juroSimplesDeterminarTaxa(valorPresente, valorFuturo, periodo)
							: Calculo.juroCompostoDeterminarTaxa(valorPresente, valorFuturo, periodo);

					txtResultado.appendText("i = " + String.format("%.2f", taxaJuros * 100) + "%\n");
					break;

				case "n":
					txtResultado.appendText("Determinando o período\n");
					valorPresente = Double.parseDouble(txtPv.getText().replace(',', '.'));
					valorFuturo = Double.parseDouble(txtFv.getText().replace(',', '.'));
					taxaJuros = Double.parseDouble(txtTaxa.getText().replace(',', '.')) / 100.0;

					periodo = (cbTipo.getValue().equals("SIMPLES"))
							? Calculo.jurosimplesDeterminarPeriodo(valorPresente, valorFuturo, taxaJuros)
							: Calculo.juroCompostoDeterminarPeriodo(valorPresente, valorFuturo, taxaJuros);

					txtResultado.appendText("n = " + String.format("%.4f", periodo) + "\n");
					break;

				default:
					txtResultado.appendText("Opção inválida para determinar o valor\n");
			}
		} catch (Exception e) {
			txtResultado.appendText("Algum campo deve estar preenchido incorretamente\n");
		}
	}

	public void limpaValores() {
		txtFv.setText("");
		txtPv.setText("");
		txtTaxa.setText("");
		txtResultado.clear();
	}
}
