package util;

public class Calculo {

    public static double juroSimplesDeterminarValorFuturo(double valorPresente, double taxa, double periodo) {
        double juros = valorPresente * taxa * periodo;
        return valorPresente + juros;
    }

    public static double juroCompostoDeterminarValorFuturo(double valorPresente, double taxa, double periodo) {
        return valorPresente * Math.pow(1 + taxa, periodo);
    }

    public static double juroSimplesDeterminarValorPresente(double valorFuturo, double taxa, double periodo) {
        return valorFuturo / (1 + taxa * periodo);
    }

    public static double juroCompostoDeterminarValorPresente(double valorFuturo, double taxa, double periodo) {
        return valorFuturo / Math.pow(1 + taxa, periodo);
    }

    public static double juroSimplesDeterminarTaxa(double valorPresente, double valorFuturo, double periodo) {
        return (valorFuturo / valorPresente - 1) / periodo;
    }

    public static double juroCompostoDeterminarTaxa(double valorPresente, double valorFuturo, double periodo) {
        return Math.pow(valorFuturo / valorPresente, 1.0 / periodo) - 1;
    }

    public static double jurosimplesDeterminarPeriodo(double valorPresente, double valorFuturo, double taxa) {
        double valor = (valorFuturo / valorPresente - 1) / taxa;
        return valor;
    }

    public static double juroCompostoDeterminarPeriodo(double valorPresente, double valorFuturo, double taxa) {
        double valor = (Math.log(valorFuturo) - Math.log(valorPresente)) / Math.log(1 + taxa);
        return valor;
    }
}
